import * as React from "react";

class HeaderComponent extends React.Component {
    public render() {
        return (
            <div className={"header"}>
                <div>Exchange</div>
            </div>
        );
    }
}

export const Header = HeaderComponent;
