import * as React from 'react';

export class DropdownHeader extends React.Component {
    public render() {
        const {children} = this.props;

        return (
            <div className={'dropdown-selected'}>
                {children}
            </div>
        );
    }
}
