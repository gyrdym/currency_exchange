import * as React from 'react';
import {IDropdownItemOwnProps} from './dropdownItem.interface';

export class DropdownItem extends React.Component<IDropdownItemOwnProps> {
    public render() {
        const {children, onClick} = this.props;

        return (
            <div onClick={() => onClick()} className={'dropdown-option'}>
                {children}
            </div>
        );
    }
}
