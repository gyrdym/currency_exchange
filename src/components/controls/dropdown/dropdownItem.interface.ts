export interface IDropdownItemOwnProps {
    onClick: () => void;
}
