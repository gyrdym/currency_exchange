import * as React from 'react';

export class DropdownContent extends React.Component {
    public render() {
        const {children} = this.props;

        return (
            <div className={'dropdown-content'}>
                {children}
            </div>
        );
    }
}
