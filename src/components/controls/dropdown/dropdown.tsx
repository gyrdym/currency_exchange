import * as React from 'react';
import {generateId} from '../../../helpers/generateId';
import {IDropdownState} from "./dropdown.interface";

export class Dropdown extends React.Component<{}, IDropdownState> {
    public state = {
        isOpened: false,
    };

    private rootId: string = generateId('dropdown-');

    public componentDidMount(): void {
        document.addEventListener('click', (event) => this.onDocumentClick(event));
    }

    public render() {
        const {children} = this.props;

        return (
            <div className={'dropdown-root'} id={this.rootId}>
                <div className={'dropdown-header'} onClick={this.toggleOpenedState}>
                    <div className={'dropdown-header-content'}>
                        {children[0]}
                    </div>
                    <div className={this.getIconCssClass()}/>
                </div>
                <div
                    className={`dropdown-options${this.getOpenedStateClassName()}`}
                    onClick={this.toggleOpenedState}
                >
                    {children[1]}
                </div>
            </div>
        );
    }

    private toggleOpenedState = () => {
        this.setState({
            isOpened: !this.state.isOpened,
        });
    }

    private getOpenedStateClassName = () => {
        return this.state.isOpened ? '' : ' hidden';
    }

    private getIconCssClass() {
        return this.state.isOpened ? 'icon-collapse' : 'icon-expand';
    }

    private onDocumentClick(event: MouseEvent) {
        const {target} = event;
        const targetEl = target as HTMLElement;

        const wasClickOnTheElement = targetEl.closest(`#${this.rootId}`);

        if (wasClickOnTheElement) {
            return;
        }

        if (this.state.isOpened) {
            this.toggleOpenedState();
        }
    }
}
