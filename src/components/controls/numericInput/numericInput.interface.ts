export interface INumericInputOwnProps {
    value?: number;
    onChange?: (value: number) => void;
    decimalSeparator?: string;
    digitsAfterDot?: number;
}

export interface INumericInputState {
    trailingDecimalPart: string;
}
