import * as React from 'react';
import {defaultDecimalSeparator, defaultDecimalDigits} from '../../../common/common.constants';
import {formatNumericValue} from '../../../helpers/formatNumericValue';
import {getTrailingDecimalPart} from '../../../helpers/getTrailingDecimalPart';
import {purifyNumericValue} from '../../../helpers/purifyNumericValue';
import {toNumber} from '../../../helpers/toNumber';
import {INumericInputOwnProps, INumericInputState} from './numericInput.interface';

export class NumericInput extends React.Component<INumericInputOwnProps, INumericInputState> {
    public state = {
        trailingDecimalPart: '',
    };

    public render() {
        const {trailingDecimalPart} = this.state;
        const {value, digitsAfterDot = defaultDecimalDigits, decimalSeparator = defaultDecimalSeparator} = this.props;

        const formattedValue = formatNumericValue(value, {
            decimalSeparator,
            maxDecimalDigits: digitsAfterDot,
        });

        const fullValue = `${formattedValue}${trailingDecimalPart}`;

        return (
            <input
                className={'textfield'}
                type='text'
                value={fullValue}
                onChange={this.onChange}
            />
        );
    }

    private onChange = ({target: {value: rawValue}}: React.ChangeEvent<HTMLInputElement>) => {
        const {
            onChange,
            decimalSeparator = defaultDecimalSeparator,
            digitsAfterDot = defaultDecimalDigits,
        } = this.props;

        const purifiedValue = purifyNumericValue(rawValue, {
            decimalSeparator,
            maxDecimalDigits: digitsAfterDot,
        });

        if (onChange) {
            const value = toNumber(purifiedValue, decimalSeparator);
            onChange(value);
        }

        const trailingDecimalPart = getTrailingDecimalPart(purifiedValue, {
            decimalSeparator,
            maxDigitsAfterDot: digitsAfterDot,
        });

        this.setState({
            trailingDecimalPart,
        });
    }
}
