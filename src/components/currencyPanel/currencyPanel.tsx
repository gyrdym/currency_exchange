import * as React from 'react';
import {connect} from 'react-redux';
import {currencyToSymbolMap} from '../../common/common.constants';
import {formatMoney} from '../../helpers/formatMoney';
import {Dropdown} from '../controls/dropdown/dropdown';
import {DropdownContent} from '../controls/dropdown/dropdownContent';
import {DropdownHeader} from '../controls/dropdown/dropdownHeader';
import {DropdownItem} from '../controls/dropdown/dropdownItem';
import {NumericInput} from '../controls/numericInput/numericInput';
import {ICurrencyPanelProps} from './currencyPanel.interface';
import {currencyPanelSelector} from './currencyPanel.selector';

class CurrencyPanelComponent extends React.Component<ICurrencyPanelProps> {
    public render() {
        const {
            currencyCodes,
            selectedCurrencyCode,
            formattedBalance,
            sum = 0,
        } = this.props;

        if (currencyCodes.length === 0) {
            return null;
        }

        const currencySymbol = currencyToSymbolMap[selectedCurrencyCode];

        return (
            <div className={'currency-panel-root'}>
                <div className={'left-block'}>
                    <Dropdown>
                        <DropdownHeader>
                            {selectedCurrencyCode}
                        </DropdownHeader>
                        <DropdownContent>
                            {this.renderOptions()}
                        </DropdownContent>
                    </Dropdown>
                    <div className={'balance-label'}>
                        Balance: {formattedBalance} {currencySymbol}
                    </div>
                </div>
                <div className={'right-block'}>
                    <NumericInput value={sum} onChange={this.onSumChange} />
                </div>
            </div>
        );
    }

    private renderOptions() {
        const {currencyCodes, accounts} = this.props;

        return currencyCodes.map((code, index) => {
            const balance = accounts[code]?.balance || 0;
            const formattedBalance = formatMoney(balance);

            return (
                <DropdownItem key={index} onClick={this.onOptionChange(code)}>
                    <div className={'currency'}>{code}</div> <div className={'balance'}>{formattedBalance}</div>
                </DropdownItem>
            );
        });
    }

    private onOptionChange = (selectedCurrencyCode: string) => () => {
        const {onCurrencySelected} = this.props;

        if (onCurrencySelected) {
            onCurrencySelected(selectedCurrencyCode);
        }
    }

    private onSumChange = (value: number) => {
        const {onSumChange} = this.props;

        if (onSumChange) {
            onSumChange(value);
        }
    }
}

export const CurrencyPanel = connect(currencyPanelSelector)(CurrencyPanelComponent);
