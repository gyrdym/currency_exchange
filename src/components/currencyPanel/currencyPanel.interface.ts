import {IAccountsData} from '../../redux/accounts/store/store.interface';

export interface ICurrencyPanelOwnProps {
    sum?: number;
    currencyCodes: string[];
    onCurrencySelected?: (currencyCode: string) => void;
    onSumChange?: (value: number) => void;
    selectedCurrencyCode?: string;
}

export interface ICurrencyPanelStoreProps {
    accounts: IAccountsData;
    formattedBalance: string;
}

export type ICurrencyPanelProps = ICurrencyPanelOwnProps & ICurrencyPanelStoreProps;
