import {formatMoney} from '../../helpers/formatMoney';
import {IStore} from '../../redux/app/store/store.interface';
import {ICurrencyPanelOwnProps, ICurrencyPanelStoreProps} from './currencyPanel.interface';

export function currencyPanelSelector(state: IStore, ownProps: ICurrencyPanelOwnProps): ICurrencyPanelStoreProps {
    const {selectedCurrencyCode} = ownProps;

    return {
        accounts: state.accounts.data,
        formattedBalance: formattedBalanceSelector(state, {currencyCode: selectedCurrencyCode}),
    };
}

export function formattedBalanceSelector(state: IStore, {currencyCode}: {currencyCode: string}) {
    const balance = (!currencyCode || !state.accounts.data[currencyCode])
        ? 0
        : state.accounts.data[currencyCode].balance;

    return formatMoney(balance);
}
