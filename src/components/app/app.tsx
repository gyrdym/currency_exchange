import * as React from 'react';
import {hot} from 'react-hot-loader';
import {connect} from 'react-redux';

import '../../assets/scss/app.scss';
import {defaultMinSum} from '../../common/common.constants';
import {getSafeNextIndex} from '../../helpers/getSafeNextIndex';
import {roundFloat} from '../../helpers/roundFloat';
import {initActionCreator} from '../../redux/app/actions/initActionCreator';
import {exchangeActionCreator} from '../../redux/exchange/actions/exchangeActionCreator';
import {setSourceCurrency} from '../../redux/exchange/actions/setSourceCurrency.action';
import {setTargetCurrency} from '../../redux/exchange/actions/setTargetCurrency.action';
import {ratesIntervalRequestActionCreator} from '../../redux/rates/actions/ratesIntervalRequestActionCreator';
import {IRatesData} from '../../redux/rates/store/store.interface';
import {CurrencyPanel} from '../currencyPanel/currencyPanel';
import {Header} from '../header/header';
import {Toolbar} from '../toolbar/toolbar';
import {IAppProps, IAppState} from './app.interface';
import {appSelector} from './app.selector';

class AppComponent extends React.Component<IAppProps, IAppState> {
    public state = {
        lastInput: undefined,
        sourceCurrencySum: 0.01,
        targetCurrencySum: 0,
    };

    public componentDidMount(): void {
        this.props.initApp();
    }

    public componentDidUpdate(
        {rates: nextRates, targetCurrencyCode: nextTargetCurrencyCode}: IAppProps,
    ) {
        const {sourceCurrencyCode, currencyCodes} = this.props;

        if (currencyCodes.length === 0) {
            return;
        }

        if (!sourceCurrencyCode) {
            this.setInitialCurrencyCodes();
            return;
        }

        this.updateConversion(nextRates, nextTargetCurrencyCode);
    }

    public render() {
        const {
            currencyCodes,
            targetCurrencyCode,
            sourceCurrencyCode,
            isLoadingInProgress,
            rates,
        } = this.props;

        const {
            sourceCurrencySum,
            targetCurrencySum,
            lastInput,
        } = this.state;

        if (isLoadingInProgress) {
            return null;
        }

        const warningMessage = this.getWarningMessage();
        const warningBlock = warningMessage && lastInput !== undefined
            ? (<div className={'warning'}>{warningMessage}</div>)
            : '';

        const rate = rates[targetCurrencyCode];

        return (
            <div className='app'>
                <Header />
                <CurrencyPanel
                    currencyCodes={currencyCodes.filter(code => code !== targetCurrencyCode)}
                    selectedCurrencyCode={sourceCurrencyCode}
                    sum={roundFloat(sourceCurrencySum)}
                    onCurrencySelected={this.setSourceCurrencyCode}
                    onSumChange={this.onSourceCurrencySumChange}
                />
                <Toolbar
                    onChangeClick={this.onCurrenciesChangeClick}
                    rate={rate}
                    targetCurrencyCode={targetCurrencyCode}
                    sourceCurrencyCode={sourceCurrencyCode}
                />
                <CurrencyPanel
                    currencyCodes={currencyCodes.filter(code => code !== sourceCurrencyCode)}
                    showButton={true}
                    selectedCurrencyCode={targetCurrencyCode}
                    sum={roundFloat(targetCurrencySum)}
                    onCurrencySelected={this.setTargetCurrencyCode}
                    onSumChange={this.onTargetCurrencySumChange}
                />
                <div className={'button-bar'}>
                    <button
                        disabled={!!warningMessage}
                        onClick={this.onExchangeClick}
                    >
                        Exchange
                    </button>
                    {warningBlock}
                </div>
            </div>
        );
    }

    private setInitialCurrencyCodes() {
        const {depositCurrency, currencyCodes} = this.props;

        this.props.setSourceCurrency(depositCurrency);
        const targetCurrencyIndex = getSafeNextIndex(currencyCodes, depositCurrency);
        this.props.setTargetCurrency(currencyCodes[targetCurrencyIndex]);
    }

    private updateConversion(nextRates: IRatesData, nextTargetCurrencyCode: string) {
        const {rates, targetCurrencyCode} = this.props;
        const {sourceCurrencySum} = this.state;

        const ratesChanged = rates[targetCurrencyCode] !== nextRates[targetCurrencyCode];
        const targetCurrencyChanged = targetCurrencyCode !== nextTargetCurrencyCode;

        if (ratesChanged || targetCurrencyChanged) {
            this.convertCurrency(sourceCurrencySum, targetCurrencyCode);
        }
    }

    private setSourceCurrencyCode = (currencyCode: string) => {
        this.props.setSourceCurrency(currencyCode);
        this.props.fetchRates(currencyCode);
    };

    private setTargetCurrencyCode = (currencyCode: string) => {
        this.props.setTargetCurrency(currencyCode);
        this.setState({
            lastInput: 'target',
        });
    };

    private onSourceCurrencySumChange = (sourceCurrencySum: number) => {
        const {targetCurrencyCode} = this.props;

        this.convertCurrency(sourceCurrencySum, targetCurrencyCode);
        this.setState({
            lastInput: 'source',
        });
    };

    private onTargetCurrencySumChange = (targetCurrencySum: number) => {
        const {targetCurrencyCode} = this.props;

        this.convertCurrency(targetCurrencySum, targetCurrencyCode, true);
    };

    private convertCurrency(sourceCurrencySum: number, targetCurrencyCode: string, isBackConversion = false) {
        const {rates} = this.props;
        const rate = rates[targetCurrencyCode];

        const targetCurrencySum = isBackConversion
            ? sourceCurrencySum / rate
            : sourceCurrencySum * rate;

        this.setState({
            sourceCurrencySum: isBackConversion
                ? targetCurrencySum
                : sourceCurrencySum,
            targetCurrencySum: isBackConversion
                ? sourceCurrencySum
                : targetCurrencySum,
        });
    }

    private onCurrenciesChangeClick = () => {
        const {targetCurrencyCode, sourceCurrencyCode} = this.props;
        const {targetCurrencySum, sourceCurrencySum} = this.state;

        this.setState({
            sourceCurrencySum: targetCurrencySum,
            targetCurrencySum: sourceCurrencySum,
        });

        this.props.setSourceCurrency(targetCurrencyCode);
        this.props.setTargetCurrency(sourceCurrencyCode);
        this.props.fetchRates(targetCurrencyCode);
    };

    private getWarningMessage(): string {
        const {sourceCurrencyCode} = this.props;
        const {sourceCurrencySum} = this.state;
        const {accounts} = this.props;

        if (this.targetSum < defaultMinSum) {
            return 'Please enter a valid amount';
        }

        if (sourceCurrencySum > (accounts[sourceCurrencyCode]?.balance || 0)) {
            return 'Not enough funds';
        }

        return '';
    }

    private onExchangeClick = () => {
        const {targetCurrencyCode, sourceCurrencyCode} = this.props;
        const {sourceCurrencySum, targetCurrencySum} = this.state;

        this.props.exchange(sourceCurrencyCode, sourceCurrencySum, targetCurrencyCode, targetCurrencySum);

        this.setState({
            lastInput: undefined,
            sourceCurrencySum: 0,
            targetCurrencySum: 0,
        });
    }

    private get targetSum() {
        const {lastInput, sourceCurrencySum, targetCurrencySum} = this.state;
        return lastInput === 'target' ? sourceCurrencySum : targetCurrencySum;
    }
}

export const App = connect(
    appSelector,
    {
        exchange: exchangeActionCreator,
        fetchRates: ratesIntervalRequestActionCreator,
        initApp: initActionCreator,
        setSourceCurrency,
        setTargetCurrency,
    },
)(AppComponent);

declare let module: object;

export default hot(module)(App);
