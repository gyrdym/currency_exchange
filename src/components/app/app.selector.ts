import {IStore} from '../../redux/app/store/store.interface';
import {IAppStoreProps} from './app.interface';

export const appSelector = (state: IStore): IAppStoreProps => {
    const currencyCodes = Object.keys(state.rates.data);

    return {
        accounts: state.accounts.data,
        currencyCodes,
        depositCurrency: state.profile.depositCurrency,
        isLoadingInProgress: state.accounts.areAccountsFetching || state.profile.isProfileFetching,
        rates: state.rates.data,
        sourceCurrencyCode: state.exchange.sourceCurrencyCode,
        targetCurrencyCode: state.exchange.targetCurrencyCode,
    };
};
