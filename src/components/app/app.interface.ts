import {IAction} from '../../common/common.interface';
import {IAccountsData} from '../../redux/accounts/store/store.interface';
import {IRatesData} from '../../redux/rates/store/store.interface';

export interface IAppDispatchProps {
    exchange: (
        currencyCode: string,
        currencySum: number,
        targetCurrencyCode: string,
        targetCurrencySum: number,
    ) => {type: string};
    fetchRates: (currencyCode: string) => IAction<string>;
    initApp: () => IAction<void>;
    setSourceCurrency: (currencyCode: string) => IAction<string>;
    setTargetCurrency: (currencyCode: string) => IAction<string>;
}

export interface IAppStoreProps {
    isLoadingInProgress: boolean;
    accounts: IAccountsData;
    rates: IRatesData;
    currencyCodes: string[];
    depositCurrency: string;
    targetCurrencyCode: string;
    sourceCurrencyCode: string;
}

export interface IAppState {
    sourceCurrencySum: number;
    targetCurrencySum: number;
    lastInput?: 'target' | 'source';
}

export type IAppProps = IAppDispatchProps & IAppStoreProps;
