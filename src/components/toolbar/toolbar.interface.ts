export interface IToolbarOwnProps {
    onChangeClick: () => void;
    rate: number;
    sourceCurrencyCode: string;
    targetCurrencyCode: string;
}
