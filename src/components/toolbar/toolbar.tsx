import * as React from 'react';
import {currencyToSymbolMap} from '../../common/common.constants';
import {formatNumericValue} from '../../helpers/formatNumericValue';
import {IToolbarOwnProps} from './toolbar.interface';

class ToolbarComponent extends React.Component<IToolbarOwnProps> {
    public render() {
        const {rate, targetCurrencyCode, sourceCurrencyCode} = this.props;

        if (!rate) {
            return null;
        }

        const targetSymbol = currencyToSymbolMap[targetCurrencyCode];
        const sourceSymbol = currencyToSymbolMap[sourceCurrencyCode];
        const formattedRate = formatNumericValue(rate, {maxDecimalDigits: 3});

        return (
            <div className={'toolbar'}>
                <div className={'change'} onClick={this.onChangeClick} />
                <div className={'rate'}>
                    <div className={'rate-icon'}/>
                    1 {sourceSymbol} = {formattedRate} {targetSymbol}
                </div>
            </div>
        );
    }

    private onChangeClick = () => {
        const {onChangeClick} = this.props;

        if (onChangeClick) {
            onChangeClick();
        }
    }
}

export const Toolbar = ToolbarComponent;
