export function getSafeNextIndex(array: any[], element: any): number {
    if (!array || array.length === 0 || array.length === 1) {
        return -1;
    }

    const elementIndex = array.findIndex(value => value === element);

    if (elementIndex === -1) {
        return -1;
    }

    return ((elementIndex + 1) % array.length);
}
