import {generateRandomNumber} from './generateRandomNumber';

const idsRegistry = new Set();

export function generateId(prefix?: string): string {
    let id: string;

    do {
        id = `${prefix}${generateRandomNumber(0, 1e10)}`;
    } while (idsRegistry.has(id));

    return id;
}
