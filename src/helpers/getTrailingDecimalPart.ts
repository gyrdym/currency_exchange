import {defaultDecimalDigits, defaultDecimalSeparator} from '../common/common.constants';

export function getTrailingDecimalPart(value: string, {
    decimalSeparator = defaultDecimalSeparator,
    maxDigitsAfterDot = defaultDecimalDigits,
} = {}) {
    if (!value) {
        return '';
    }

    const [, decimalPart] = value.split(decimalSeparator);

    if (decimalPart === undefined) {
        return '';
    }

    if (decimalPart === '') {
        return decimalSeparator;
    }

    if (/^0*$/.test(decimalPart)) {
        return `${decimalSeparator}${decimalPart.substring(0, maxDigitsAfterDot)}`;
    }

    const trimmedDecimalPart = decimalPart.substring(0, maxDigitsAfterDot);

    if (trimmedDecimalPart.endsWith('0')) {
        const indexOfZero = trimmedDecimalPart.indexOf('0');

        if (indexOfZero !== -1) {
            return trimmedDecimalPart.substring(indexOfZero);
        }
    }

    return '';
}
