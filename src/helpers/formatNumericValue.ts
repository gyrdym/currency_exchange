import {defaultDecimalDigits, defaultDecimalSeparator} from '../common/common.constants';
import {roundFloat} from './roundFloat';

export function formatNumericValue(value: number, {
    decimalSeparator = defaultDecimalSeparator,
    maxDecimalDigits = defaultDecimalDigits,
} = {}): string {
    if (value === null || isNaN(value)) {
        return '–';
    }

    const roundedValue = roundFloat(value, maxDecimalDigits);
    const [integerPart = '', decimalPart = ''] = String(roundedValue).split('.');
    const shortenedDecimalPart = decimalPart.substring(0, maxDecimalDigits);

    if (!decimalPart) {
        return `${integerPart}`;
    }

    return `${integerPart}${decimalSeparator}${shortenedDecimalPart}`;
}
