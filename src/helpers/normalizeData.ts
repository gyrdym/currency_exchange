export function normalizeData<T>(data: T[], getKey: (obj: T) => string): {[key: string]: T} {
    return data.reduce((result, item) => ({
        ...result,
        [getKey(item)]: item,
    }), {});
}
