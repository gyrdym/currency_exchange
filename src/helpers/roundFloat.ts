import {defaultDecimalDigits} from '../common/common.constants';

export function roundFloat(value: number, maxDecimalDigits = defaultDecimalDigits) {
    return Math.round(value * Math.pow(10,  maxDecimalDigits)) / Math.pow(10,  maxDecimalDigits);
}
