import {defaultDecimalSeparator} from '../common/common.constants';

export function toNumber(value: string, decimalSeparator = defaultDecimalSeparator): number {
    if (!value) {
        return 0;
    }
    return parseFloat(value.replace(decimalSeparator, '.'));
}
