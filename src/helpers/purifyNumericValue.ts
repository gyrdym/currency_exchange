import {defaultDecimalDigits, defaultDecimalSeparator} from '../common/common.constants';

export function purifyNumericValue(value: string, {
    decimalSeparator = defaultDecimalSeparator,
    maxDecimalDigits = defaultDecimalDigits,
} = {}): string {
    if (!value) {
        return '';
    }

    const purifiedValue = value
        .trim()
        // remove all the non-numeric characters (except for decimal separator character)
        .replace(new RegExp(`[^0-9${decimalSeparator}]`, 'g'), '')

        // remove all the non-numeric characters from the beginning of the string
        .replace(/^(\D)*/, '')

        // remove duplicated zeroes from the beginning of the string
        .replace(/^0+/, '0')

        // remove the first zero if a non-zero numeric character follows the first zero
        .replace(/^0+(\d)/, '$1')

        // replace decimal separator character with a dot char
        .replace(decimalSeparator, '.');

    const fractionalNumberPattern = /^\d*\.?\d*/;
    const [purifiedFractionalValue = ''] = purifiedValue.match(fractionalNumberPattern) || [];
    const [integerPart, decimalPart] = purifiedFractionalValue.split('.');

    if (decimalPart === undefined) {
        return integerPart;
    }

    return `${integerPart}${decimalSeparator}${decimalPart.substring(0, maxDecimalDigits)}`;
}
