import {generateRandomNumber} from '../../helpers/generateRandomNumber';
import {currencies, generateRandomCurrency} from '../helpers/generateRandomCurrency';
import {IGetAccountResponseModel} from './getAccounts.interface';

export async function getAccounts(): Promise<IGetAccountResponseModel> {
    const numberOfAccounts = generateRandomNumber(3, currencies.length);
    const emptyAccounts = new Array(numberOfAccounts).fill(undefined);
    const addedCurrencies = new Set();

    return emptyAccounts.map(() => {
        let currency;

        do {
            currency = generateRandomCurrency();
        } while (addedCurrencies.has(currency));

        return {
            balance: generateRandomNumber(0.1, 1000),
            currencyCode: currency,
        };
    });
}
