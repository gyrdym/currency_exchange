export type IGetAccountResponseModel = IAccountResponseModel[];

export interface IAccountResponseModel {
    currencyCode: string;
    balance: number;
}
