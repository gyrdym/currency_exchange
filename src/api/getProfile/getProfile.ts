import {generateRandomCurrency} from '../helpers/generateRandomCurrency';
import {IGetProfileResponseModel} from './getProfile.interface';

export async function getProfile(): Promise<IGetProfileResponseModel> {
    const depositCurrency = generateRandomCurrency();

    return {
        depositCurrency,
    };
}
