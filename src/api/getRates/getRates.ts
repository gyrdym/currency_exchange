import {IGetRatesResponseModel, IRatesResponseModel} from './getRates.interface';

export async function getRates(depositCurrency: string): Promise<IRatesResponseModel> {
    const response = await fetch(`https://api.exchangeratesapi.io/latest?base=${depositCurrency}`);
    const decoded = await response.json() as IGetRatesResponseModel;

    return decoded.rates;
}
