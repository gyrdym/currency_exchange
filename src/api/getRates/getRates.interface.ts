export interface IGetRatesResponseModel {
    rates: IRatesResponseModel;
    base: string;
    date: string;
}

export interface IRatesResponseModel {
    [key: string]: number;
}
