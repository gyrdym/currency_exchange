import {generateRandomNumber} from '../../helpers/generateRandomNumber';

export const currencies = [
    'CAD', 'HKD', 'ISK', 'PHP', 'DKK', 'HUF', 'CZK', 'GBP', 'RON', 'SEK', 'IDR', 'BRL', 'RUB', 'HRK', 'JPY', 'THB',
    'CHF', 'EUR', 'MYR', 'BGN', 'TRY', 'CNY', 'NOK', 'NZD', 'ZAR', 'USD', 'MXN', 'SGD', 'AUD', 'ILS', 'KRW', 'PLN',
];


export function generateRandomCurrency() {
    const currencyIdx = generateRandomNumber(0, currencies.length - 1);

    return  currencies[currencyIdx];
}
