export interface IAction<P> {
    payload?: P;
    type: string;
}
