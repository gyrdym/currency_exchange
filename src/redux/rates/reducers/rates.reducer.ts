import {RATES_INTERVAL_REQUEST_STARTED} from "../actions/ratesIntervalRequestStarted.action";
import {RATES_REQUEST_FAILED} from '../actions/ratesRequestFailed.action';
import {RATES_REQUEST_STARTED} from '../actions/ratesRequestStarted.action';
import {RATES_REQUEST_SUCCEEDED} from '../actions/ratesRequestSucceeded.action';
import {ratesStoreInitialState} from '../store/store.constants';
import {IRatesStore} from '../store/store.interface';

export function ratesReducer(state: IRatesStore = ratesStoreInitialState, action): IRatesStore {
    switch (action.type) {
        case RATES_REQUEST_STARTED:
            return {
                ...state,
                areRatesFetching: true,
            };

        case RATES_REQUEST_SUCCEEDED:
            return {
                ...state,
                areRatesFetching: false,
                data: action.payload || {},
            };

        case RATES_REQUEST_FAILED:
            return {
                ...state,
                areRatesFetching: false,
            };

        case RATES_INTERVAL_REQUEST_STARTED:
            return {
                ...state,
                intervalRequestFor: action.payload.currencyCode,
                intervalRequestId: action.payload.intervalId,
            };

        default:
            return state;
    }
}
