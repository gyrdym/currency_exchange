export interface IRatesStoreSegment {
    rates: IRatesStore;
}

export interface IRatesStore {
    data: IRatesData;
    areRatesFetching: boolean;
    intervalRequestId: number;
    intervalRequestFor: string;
}

export interface IRatesData {
    [key: string]: number;
}
