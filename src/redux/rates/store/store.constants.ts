import {IRatesStore} from './store.interface';

export const ratesStoreInitialState: IRatesStore = {
    areRatesFetching: false,
    data: {},
    intervalRequestFor: '',
    intervalRequestId: -1,
};
