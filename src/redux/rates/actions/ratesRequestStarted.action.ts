export const RATES_REQUEST_STARTED = 'RATES_REQUEST_STARTED';
export function fetchRatesRequestStarted() {
    return {
        type: RATES_REQUEST_STARTED,
    };
}
