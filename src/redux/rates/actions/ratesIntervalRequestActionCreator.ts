import {ratesIntervalRequestStarted} from './ratesIntervalRequestStarted.action';
import {ratesRequestActionCreator} from './ratesRequestActionCreator';

export function ratesIntervalRequestActionCreator(baseCurrency?: string) {
    return (dispatch, getState) => {
        const state = getState();
        const {intervalRequestId, intervalRequestFor} = state.rates;
        const currencyCode = state.exchange.sourceCurrencyCode || baseCurrency;

        if (intervalRequestId !== -1) {
            if (intervalRequestFor === currencyCode) {
                return;
            }
            clearInterval(intervalRequestId);
        }

        dispatch(ratesRequestActionCreator(currencyCode));

        const intervalId = setInterval(() => {
            dispatch(ratesRequestActionCreator(currencyCode));
        }, 10 * 1000);

        dispatch(ratesIntervalRequestStarted(intervalId, currencyCode));
    };
}
