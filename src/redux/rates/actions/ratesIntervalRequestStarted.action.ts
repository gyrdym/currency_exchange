export const RATES_INTERVAL_REQUEST_STARTED = 'RATES_INTERVAL_REQUEST_STARTED';
export function ratesIntervalRequestStarted(intervalId, currencyCode) {
    return {
        payload: {
            currencyCode,
            intervalId,
        },
        type: RATES_INTERVAL_REQUEST_STARTED,
    };
}
