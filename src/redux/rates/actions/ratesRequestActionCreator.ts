import {getRates} from '../../../api/getRates/getRates';
import {fetchRatesRequestFailed} from './ratesRequestFailed.action';
import {fetchRatesRequestStarted} from './ratesRequestStarted.action';
import {fetchRatesRequestSucceeded} from './ratesRequestSucceeded.action';

export function ratesRequestActionCreator(baseCurrencyCode) {
    return async dispatch => {
        dispatch(fetchRatesRequestStarted());

        try {
            dispatch(fetchRatesRequestSucceeded(await getRates(baseCurrencyCode)));
        } catch {
            dispatch(fetchRatesRequestFailed());
        }
    };
}
