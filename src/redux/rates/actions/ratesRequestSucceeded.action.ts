import {IRatesResponseModel} from '../../../api/getRates/getRates.interface';

export const RATES_REQUEST_SUCCEEDED = 'RATES_REQUEST_SUCCEEDED';
export function fetchRatesRequestSucceeded(rates: IRatesResponseModel) {
    return {
        payload: rates,
        type: RATES_REQUEST_SUCCEEDED,
    };
}
