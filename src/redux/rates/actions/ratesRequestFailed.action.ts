export const RATES_REQUEST_FAILED = 'RATES_REQUEST_FAILED';
export function fetchRatesRequestFailed() {
    return {
        type: RATES_REQUEST_FAILED,
    };
}
