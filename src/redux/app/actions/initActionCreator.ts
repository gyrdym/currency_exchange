import {getProfile} from '../../../api/getProfile/getProfile';
import {accountsRequestActionCreator} from '../../accounts/actions/accountRequestActionCreator';
import {profileRequestFailed} from '../../profile/actions/profileRequestFailed.action';
import {profileRequestStarted} from '../../profile/actions/profileRequestStarted.action';
import {profileRequestSucceeded} from '../../profile/actions/profileRequestSucceeded.action';
import {ratesIntervalRequestActionCreator} from '../../rates/actions/ratesIntervalRequestActionCreator';

export function initActionCreator() {
    return dispatch => {
        dispatch(profileRequestStarted());

        getProfile()
            .then(profile => {
                dispatch(profileRequestSucceeded(profile));
                dispatch(ratesIntervalRequestActionCreator(profile.depositCurrency));
                dispatch(accountsRequestActionCreator());
            })
            .catch(() => {
                dispatch(profileRequestFailed());
            });
    };
}
