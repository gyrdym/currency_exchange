import {combineReducers} from 'redux';
import {accountsReducer} from '../../accounts/reducers/accounts.reducer';
import {exchangeReducer} from '../../exchange/reducers/exchange.reducer';
import {profileReducer} from '../../profile/reducers/profile.reducer';
import {ratesReducer} from '../../rates/reducers/rates.reducer';

export const rootReducer = combineReducers({
    accounts: accountsReducer,
    exchange: exchangeReducer,
    profile: profileReducer,
    rates: ratesReducer,
});
