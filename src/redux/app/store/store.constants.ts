import {accountsStoreInitialState} from '../../accounts/store/store.constants';
import {profileStoreInitialState} from '../../profile/store/store.constants';
import {ratesStoreInitialState} from '../../rates/store/store.constants';
import {IStore} from './store.interface';
import {exchangeStoreInitialState} from '../../exchange/store/store.constants';

export const storeInitialState: IStore = {
    accounts: accountsStoreInitialState,
    profile: profileStoreInitialState,
    rates: ratesStoreInitialState,
    exchange: exchangeStoreInitialState,
};
