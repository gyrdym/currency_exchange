import {IAccountsStoreSegment} from '../../accounts/store/store.interface';
import {IExchangeStoreSegment} from '../../exchange/store/store.interface';
import {IProfiledStoreSegment} from '../../profile/store/store.interface';
import {IRatesStoreSegment} from '../../rates/store/store.interface';

export type IStore = IAccountsStoreSegment & IRatesStoreSegment & IProfiledStoreSegment & IExchangeStoreSegment;
