import {applyMiddleware, createStore} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {rootReducer} from '../reducers/root.reducer';
import {storeInitialState} from './store.constants';
import {IStore} from './store.interface';

export function configureStore(preloadedState: IStore = storeInitialState) {
    const middlewareEnhancer = applyMiddleware(thunkMiddleware);

    return createStore(rootReducer, preloadedState, middlewareEnhancer);
}
