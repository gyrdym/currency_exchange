import {PROFILE_REQUEST_FAILED} from '../actions/profileRequestFailed.action';
import {PROFILE_REQUEST_STARTED} from '../actions/profileRequestStarted.action';
import {PROFILE_REQUEST_SUCCEEDED} from '../actions/profileRequestSucceeded.action';
import {profileStoreInitialState} from '../store/store.constants';
import {IProfileStore} from '../store/store.interface';

export function profileReducer(state: IProfileStore = profileStoreInitialState, action): IProfileStore {
    switch (action.type) {
        case PROFILE_REQUEST_STARTED:
            return {
                ...state,
                isProfileFetching: true,
            };

        case PROFILE_REQUEST_SUCCEEDED:
            return {
                ...state,
                depositCurrency: action.payload.depositCurrency,
                isProfileFetching: false,
            };

        case PROFILE_REQUEST_FAILED:
            return {
                ...state,
                isProfileFetching: false,
            };

        default:
            return {
                ...state,
            };
    }
}
