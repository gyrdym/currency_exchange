export interface IProfiledStoreSegment {
    profile: IProfileStore;
}

export interface IProfileStore {
    depositCurrency: string;
    isProfileFetching: boolean;
}
