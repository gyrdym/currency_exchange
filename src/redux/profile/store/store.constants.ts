import {IProfileStore} from './store.interface';

export const profileStoreInitialState: IProfileStore = {
    depositCurrency: 'EUR',
    isProfileFetching: false,
};
