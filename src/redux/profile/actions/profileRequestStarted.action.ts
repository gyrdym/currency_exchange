export const PROFILE_REQUEST_STARTED = 'PROFILE_REQUEST_STARTED';
export function profileRequestStarted() {
    return {
        type: PROFILE_REQUEST_STARTED,
    };
}
