export const PROFILE_REQUEST_FAILED = 'PROFILE_REQUEST_FAILED';
export function profileRequestFailed() {
    return {
        type: PROFILE_REQUEST_FAILED,
    };
}
