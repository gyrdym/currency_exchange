import {IGetProfileResponseModel} from "../../../api/getProfile/getProfile.interface";

export const PROFILE_REQUEST_SUCCEEDED = 'PROFILE_REQUEST_SUCCEEDED';
export function profileRequestSucceeded(profileData: IGetProfileResponseModel) {
    return {
        payload: profileData,
        type: PROFILE_REQUEST_SUCCEEDED,
    };
}
