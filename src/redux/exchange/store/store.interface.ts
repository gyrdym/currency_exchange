export interface IExchangeStoreSegment {
    exchange: IExchangeStore;
}

export interface IExchangeStore {
    isExchangeInProgress: boolean;
    sourceCurrencyCode: string;
    targetCurrencyCode: string;
}
