import {IExchangeStore} from './store.interface';

export const exchangeStoreInitialState: IExchangeStore = {
    isExchangeInProgress: false,
    sourceCurrencyCode: '',
    targetCurrencyCode: '',
};
