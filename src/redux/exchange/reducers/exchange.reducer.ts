import {EXCHANGE_REQUEST_FAILED} from '../actions/exchangeRequestFailed.action';
import {EXCHANGE_REQUEST_STARTED} from '../actions/exchangeRequestStarted.action';
import {EXCHANGE_REQUEST_SUCCEEDED} from '../actions/exchangeRequestSucceeded.action';
import {SET_SOURCE_CURRENCY} from '../actions/setSourceCurrency.action';
import {SET_TARGET_CURRENCY} from '../actions/setTargetCurrency.action';
import {exchangeStoreInitialState} from '../store/store.constants';
import {IExchangeStore} from '../store/store.interface';

export function exchangeReducer(state: IExchangeStore = exchangeStoreInitialState, action): IExchangeStore {
    switch (action.type) {
        case EXCHANGE_REQUEST_STARTED:
            return {
                ...state,
                isExchangeInProgress: true,
            };

        case EXCHANGE_REQUEST_SUCCEEDED:
            return {
                ...state,
                isExchangeInProgress: false,
            };

        case EXCHANGE_REQUEST_FAILED:
            return {
                ...state,
                isExchangeInProgress: false,
            };

        case SET_SOURCE_CURRENCY:
            return {
                ...state,
                sourceCurrencyCode: action.payload,
            };

        case SET_TARGET_CURRENCY:
            return {
                ...state,
                targetCurrencyCode: action.payload,
            };

        default:
            return {
                ...state,
            };
    }
}
