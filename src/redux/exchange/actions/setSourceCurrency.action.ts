export const SET_SOURCE_CURRENCY = 'SET_SOURCE_CURRENCY';
export function setSourceCurrency(currencyCode) {
    return {
        payload: currencyCode,
        type: SET_SOURCE_CURRENCY,
    };
}
