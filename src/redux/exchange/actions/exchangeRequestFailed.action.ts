export const EXCHANGE_REQUEST_FAILED = 'EXCHANGE_REQUEST_FAILED';
export function exchangeRequestFailed() {
    return {
        type: EXCHANGE_REQUEST_FAILED,
    };
}
