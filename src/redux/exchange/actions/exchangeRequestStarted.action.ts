export const EXCHANGE_REQUEST_STARTED = 'EXCHANGE_REQUEST_STARTED';
export function exchangeRequestStarted() {
    return {
        type: EXCHANGE_REQUEST_STARTED,
    };
}
