export const SET_TARGET_CURRENCY = 'SET_TARGET_CURRENCY';
export function setTargetCurrency(currencyCode) {
    return {
        payload: currencyCode,
        type: SET_TARGET_CURRENCY,
    };
}
