import {exchange} from '../../../api/exchange/exchange';
import {exchangeRequestFailed} from './exchangeRequestFailed.action';
import {exchangeRequestStarted} from './exchangeRequestStarted.action';
import {exchangeRequestSucceeded} from './exchangeRequestSucceeded.action';

export function exchangeActionCreator(
    sourceCurrencyCode: string,
    sourceSum: number,
    targetCurrencyCode: string,
    targetSum: number,
) {
    return dispatch => {
        dispatch(exchangeRequestStarted());

        exchange(sourceCurrencyCode, sourceSum, targetCurrencyCode, targetSum)
            .then(() => dispatch(exchangeRequestSucceeded(
                sourceCurrencyCode,
                sourceSum,
                targetCurrencyCode,
                targetSum,
            )))
            .catch( () => dispatch(exchangeRequestFailed()));
    };
}
