export const EXCHANGE_REQUEST_SUCCEEDED = 'EXCHANGE_REQUEST_SUCCEEDED';
export function exchangeRequestSucceeded(
    sourceCurrencyCode: string,
    sourceSum: number,
    targetCurrencyCode: string,
    targetSum: number,
) {
    return {
        payload: {
            sourceCurrencyCode,
            sourceSum,
            targetCurrencyCode,
            targetSum,
        },
        type: EXCHANGE_REQUEST_SUCCEEDED,
    };
}
