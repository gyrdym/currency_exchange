export interface IAccountsStoreSegment {
    accounts: IAccountsStore;
}

export interface IAccountsStore {
    data: IAccountsData;
    areAccountsFetching: boolean;
}

export interface IAccountsData {
    [key: string]: IAccount;
}

export interface IAccount {
    currencyCode: string;
    balance: number;
}
