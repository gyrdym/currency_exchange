import {IAccountsStore} from './store.interface';

export const accountsStoreInitialState: IAccountsStore = {
    areAccountsFetching: false,
    data: {},
};
