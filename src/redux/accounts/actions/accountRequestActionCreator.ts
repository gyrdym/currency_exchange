import {getAccounts} from '../../../api/getAccounts/getAccounts';
import {accountsRequestFailed} from './accountsRequestFailed.action';
import {accountsRequestStarted} from './accountsRequestStarted.action';
import {accountsRequestSucceeded} from './accountsRequestSucceeded.action';

export function accountsRequestActionCreator() {
    return async dispatch => {
        dispatch(accountsRequestStarted());

        try {
            dispatch(accountsRequestSucceeded(await getAccounts()));
        } catch {
            dispatch(accountsRequestFailed());
        }
    };
}
