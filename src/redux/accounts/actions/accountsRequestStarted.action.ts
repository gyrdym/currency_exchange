export const ACCOUNTS_REQUEST_STARTED = 'ACCOUNTS_REQUEST_STARTED';
export function accountsRequestStarted() {
    return {
        type: ACCOUNTS_REQUEST_STARTED,
    };
}
