export const ACCOUNTS_REQUEST_FAILED = 'ACCOUNTS_REQUEST_FAILED';
export function accountsRequestFailed() {
    return {
        type: ACCOUNTS_REQUEST_FAILED,
    };
}
