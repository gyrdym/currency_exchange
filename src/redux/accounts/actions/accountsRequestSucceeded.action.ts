import {IAccount} from '../store/store.interface';

export const ACCOUNTS_REQUEST_SUCCEEDED = 'ACCOUNTS_REQUEST_SUCCEEDED';
export function accountsRequestSucceeded(accounts: IAccount[]) {
    return {
        payload: accounts,
        type: ACCOUNTS_REQUEST_SUCCEEDED,
    };
}
