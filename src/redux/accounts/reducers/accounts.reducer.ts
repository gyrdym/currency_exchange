import {normalizeData} from '../../../helpers/normalizeData';
import {EXCHANGE_REQUEST_SUCCEEDED} from '../../exchange/actions/exchangeRequestSucceeded.action';
import {ACCOUNTS_REQUEST_FAILED} from '../actions/accountsRequestFailed.action';
import {ACCOUNTS_REQUEST_STARTED} from '../actions/accountsRequestStarted.action';
import {ACCOUNTS_REQUEST_SUCCEEDED} from '../actions/accountsRequestSucceeded.action';
import {accountsStoreInitialState} from '../store/store.constants';
import {IAccountsStore} from '../store/store.interface';

export function accountsReducer(state: IAccountsStore = accountsStoreInitialState, action): IAccountsStore {
    switch (action.type) {
        case ACCOUNTS_REQUEST_STARTED:
            return {
                ...state,
                areAccountsFetching: true,
            };

        case ACCOUNTS_REQUEST_SUCCEEDED:
            return {
                ...state,
                areAccountsFetching: false,
                data: {
                    ...state.data,
                    ...normalizeData(action.payload, account => account.currencyCode),
                },
            };

        case EXCHANGE_REQUEST_SUCCEEDED:
            const {sourceCurrencyCode, sourceSum, targetCurrencyCode, targetSum} = action.payload;

            return {
                ...state,
                data: {
                    ...state.data,
                    [sourceCurrencyCode]: {
                        ...state.data[sourceCurrencyCode],
                        balance: (state.data[sourceCurrencyCode]?.balance || 0) - sourceSum,
                    },
                    [targetCurrencyCode]: {
                        ...state.data[targetCurrencyCode],
                        balance: (state.data[targetCurrencyCode]?.balance || 0) + targetSum,
                    },
                },
            };

        case ACCOUNTS_REQUEST_FAILED:
            return {
                ...state,
                areAccountsFetching: false,
            };

        default:
            return {
                ...state,
            };
    }
}
