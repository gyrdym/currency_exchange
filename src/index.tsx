import * as React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import App from './components/app/app';
import {configureStore} from './redux/app/store/configureStore';

const store = configureStore();

render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root'),
);
