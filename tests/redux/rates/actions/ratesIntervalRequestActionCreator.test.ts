const dispatchMock = jest.fn();
const getStateMock = jest.fn();
const ratesRequestActionCreatorMock = jest.fn();
const setIntervalMock = jest.fn();

jest
    .doMock('../../../../src/redux/rates/actions/ratesRequestActionCreator', () => ({
        ratesRequestActionCreator: ratesRequestActionCreatorMock,
    }));

import { ratesIntervalRequestActionCreator } from '../../../../src/redux/rates/actions/ratesIntervalRequestActionCreator';
import { RATES_INTERVAL_REQUEST_STARTED } from '../../../../src/redux/rates/actions/ratesIntervalRequestStarted.action';

describe('ratesIntervalRequestActionCreator', () => {
    const sourceCurrencyCode = 'EUR';
    const intervalId = 100;

    let state;

    const testBaseSuccessfulPath = (action, currencyCode?: string) => {
        action(dispatchMock, getStateMock);

        expect(ratesRequestActionCreatorMock).toBeCalledWith(currencyCode);
        expect(dispatchMock).toBeCalledWith({type: 'RATES_REQUEST_ACTION'});
        expect(window.setInterval).toBeCalledWith(expect.anything(), 10 * 1000);
        expect(dispatchMock).toBeCalledWith({
            payload: {
                currencyCode: currencyCode,
                intervalId,
            },
            type: RATES_INTERVAL_REQUEST_STARTED,
        });
        expect(dispatchMock).toBeCalledTimes(2);
    };

    beforeEach(() => {
        state = {
            exchange: {
                sourceCurrencyCode,
                targetCurrencyCode: 'RUB',
            },
            rates: {
                intervalRequestId: -1, 
                intervalRequestFor: '',
            },
        };

        getStateMock.mockReturnValue(state);
        ratesRequestActionCreatorMock.mockReturnValue({type: 'RATES_REQUEST_ACTION'});

        Object.defineProperty(window, 'clearInterval', {
            value: jest.fn(),
        });

        Object.defineProperty(window, 'setInterval', {
            value: setIntervalMock,
        });

        setIntervalMock.mockReturnValue(intervalId);
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    test('should call ratesRequestActionCreator and ratesIntervalRequestStarted', () => {
        const action = ratesIntervalRequestActionCreator();
        
        testBaseSuccessfulPath(action, sourceCurrencyCode);
    });

    test('should consider passed currency code if the source currency is unspecified in the store state', () => {
        state.exchange.sourceCurrencyCode = '';
        const baseCurrency = 'USD';
        const action = ratesIntervalRequestActionCreator(baseCurrency);
        
        testBaseSuccessfulPath(action, baseCurrency);
    });

    test('should prevent creation of a new interval if the same currency is used for rates request and the interval is already created', () => {
        state.rates = {
            intervalRequestId: 123,
            intervalRequestFor: sourceCurrencyCode,
        }

        const action = ratesIntervalRequestActionCreator();

        action(dispatchMock, getStateMock);

        expect(dispatchMock).not.toBeCalled();
        expect(window.setInterval).not.toBeCalled();
    });

    test('should cancel a previous interval and create a new one if request for a new currency is started', () => {
        state.rates = {
            intervalRequestId: 123,
            intervalRequestFor: sourceCurrencyCode,
        }

        state.exchange = {
            sourceCurrencyCode: 'GBP',
        }

        const action = ratesIntervalRequestActionCreator();

        action(dispatchMock, getStateMock);

        expect(window.clearInterval).toBeCalledWith(123);
        expect(window.clearInterval).toBeCalledTimes(1);

        expect(window.setInterval).toBeCalledTimes(1);

        expect(dispatchMock).toBeCalledWith({
            payload: {
                intervalId,
                currencyCode: 'GBP',
            },
            type: RATES_INTERVAL_REQUEST_STARTED,
        });
    });
});
