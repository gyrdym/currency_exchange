const getProfileMock = jest.fn();
const ratesIntervalRequestActionCreatorMock = jest.fn();
const accountsRequestActionCreatorMock = jest.fn();

jest
    .doMock('../../../../src/api/getProfile/getProfile', () => {
        return {
            getProfile: getProfileMock,
        };
    })
    .doMock('../../../../src/redux/rates/actions/ratesIntervalRequestActionCreator', () => {
        return {
            ratesIntervalRequestActionCreator: ratesIntervalRequestActionCreatorMock,
        };
    })
    .doMock('../../../../src/redux/accounts/actions/accountRequestActionCreator', () => {
        return {
            accountsRequestActionCreator: accountsRequestActionCreatorMock,
        };
    });

import {initActionCreator} from '../../../../src/redux/app/actions/initActionCreator';
import {PROFILE_REQUEST_FAILED} from "../../../../src/redux/profile/actions/profileRequestFailed.action";
import {PROFILE_REQUEST_STARTED} from '../../../../src/redux/profile/actions/profileRequestStarted.action';
import {PROFILE_REQUEST_SUCCEEDED} from '../../../../src/redux/profile/actions/profileRequestSucceeded.action';

const dispatchMock = jest.fn();

describe('initActionCreator', () => {
    beforeEach(() => {
        getProfileMock.mockResolvedValue({depositCurrency: 'EUR'});
        ratesIntervalRequestActionCreatorMock.mockReturnValue({type: 'RATES_REQUEST'});
        accountsRequestActionCreatorMock.mockReturnValue({type: 'ACCOUNTS_REQUEST'});
    });

    afterEach(() => {
        jest.resetAllMocks();
    });

    test('should call profile request started action and load profile', async () => {
        const action = initActionCreator();

        action(dispatchMock);

        expect(dispatchMock).toBeCalledWith({type: PROFILE_REQUEST_STARTED});

        await Promise.resolve();

        expect(dispatchMock).toBeCalledWith({
            payload: {
                depositCurrency: 'EUR',
            },
            type: PROFILE_REQUEST_SUCCEEDED,
        });

        expect(dispatchMock).toBeCalledWith({type: 'RATES_REQUEST'});
        expect(dispatchMock).toBeCalledWith({type: 'ACCOUNTS_REQUEST'});

        expect(dispatchMock).toBeCalledTimes(4);
    });

    test('should dispatch PROFILE_REQUEST_FAILED action if getProfile request failed', async () => {
        getProfileMock.mockRejectedValueOnce('error');

        const action = initActionCreator();

        action(dispatchMock);

        expect(dispatchMock).toBeCalledWith({type: PROFILE_REQUEST_STARTED});

        // we need to wait until getProfile promise is rejected
        await Promise.resolve();
        await Promise.resolve();

        expect(dispatchMock).toBeCalledWith({
            type: PROFILE_REQUEST_FAILED,
        });

        expect(dispatchMock).toBeCalledTimes(2);
    });
});
