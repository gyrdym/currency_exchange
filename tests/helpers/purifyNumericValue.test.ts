import {purifyNumericValue} from '../../src/helpers/purifyNumericValue';

describe('purifyNumericValue', () => {
    test.each`
        value                    | decimalDigits | expected
        ${''}                    | ${2}          | ${''}
        ${undefined}             | ${2}          | ${''}
        ${null}                  | ${2}          | ${''}
        ${'123'}                 | ${2}          | ${'123'}
        ${'123,'}                | ${2}          | ${'123,'}
        ${',1'}                  | ${2}          | ${'1'}
        ${'12,1'}                | ${2}          | ${'12,1'}
        ${'abc12,1'}             | ${2}          | ${'12,1'}
        ${'a1b2,1cc'}            | ${2}          | ${'12,1'}
        ${'a1b2,456,68,1cc'}     | ${2}          | ${'12,45'}
        ${'&*1%^b44.456,68,1cc'} | ${2}          | ${'144456,68'}
        ${'000000'}              | ${2}          | ${'0'}
        ${'02'}                  | ${2}          | ${'2'}
        ${'0,0'}                 | ${2}          | ${'0,0'}
        ${'02,df34'}             | ${2}          | ${'2,34'}
        ${'02,df3444'}           | ${3}          | ${'2,344'}
        ${'0,s12df3444'}         | ${1}          | ${'0,1'}
        ${'0,112df3000'}         | ${7}          | ${'0,1123000'}
    `('should return $expected if the passed value is $value and max decimal digits is $decimalDigits',
        ({value, decimalDigits, expected}) => {
        expect(purifyNumericValue(value, {maxDecimalDigits: decimalDigits})).toBe(expected);
    });
});
