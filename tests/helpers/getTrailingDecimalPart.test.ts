import {getTrailingDecimalPart} from '../../src/helpers/getTrailingDecimalPart';

describe('getTrailingDecimalPart', () => {
    test.each`
        value              | digitsAfterDot | expected
        ${''}              | ${2}           |${''}
        ${null}            | ${2}           | ${''}
        ${undefined}       | ${2}           | ${''}
        ${'123'}           | ${2}           | ${''}
        ${'123,'}          | ${2}           | ${','}
        ${'123,1'}         | ${2}           | ${''}
        ${'123,123456789'} | ${2}           | ${''}
        ${'123,123456789'} | ${2}           | ${''}
        ${'123,10'}        | ${2}           | ${'0'}
        ${'123,10000'}     | ${2}           | ${'0'}
        ${'123,10000'}     | ${3}           | ${'00'}
        ${'123,100'}       | ${3}           | ${'00'}
        ${'123,120'}       | ${3}           | ${'0'}
        ${'0,'}            | ${2}           | ${','}
        ${'0,0'}           | ${2}           | ${',0'}
        ${'0,0000'}        | ${2}           | ${',00'}
        ${'0,0000'}        | ${4}           | ${',0000'}
        ${'0,01'}          | ${2}           | ${''}
        ${'0,01'}          | ${3}           | ${''}
        ${'0,001'}         | ${2}           | ${'00'}
    `('should return `$expected` if the passed value is $value and the maximum number of decimal digits ' +
        'is $digitsAfterDot', ({value, digitsAfterDot, expected}) => {

        expect(getTrailingDecimalPart(value, {maxDigitsAfterDot: digitsAfterDot})).toBe(expected);
    });
});
