import {getSafeNextIndex} from '../../src/helpers/getSafeNextIndex';

describe('getSafeNextIndex', () => {
    test.each`
        array                | element | expected
        ${[]}                | ${100}  | ${-1}
        ${undefined}         | ${100}  | ${-1}
        ${null}              | ${100}  | ${-1}
        ${[1]}               | ${100}  | ${-1}
        ${[1, 2, 3]}         | ${100}  | ${-1}
        ${[100]}             | ${100}  | ${-1}
        ${[100, 12]}         | ${100}  | ${1}
        ${[12, 100]}         | ${100}  | ${0}
        ${[300, 12, 100]}    | ${100}  | ${0}
        ${[100, 12, 100]}    | ${100}  | ${1}
        ${[10, 12, 100, 23]} | ${100}  | ${3}
    `('should return $expected if passed array is $array and the element to get the next index of is $element',
        ({array, element, expected}) => {

        expect(getSafeNextIndex(array, element)).toBe(expected);
    });
});
