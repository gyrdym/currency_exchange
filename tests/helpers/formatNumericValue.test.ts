import {formatNumericValue} from '../../src/helpers/formatNumericValue';

describe('formatNumericValue', () => {
    test.each`
        value        | decimalDigits | expected
        ${undefined} | ${2}          | ${'–'}
        ${null}      | ${2}          | ${'–'}
        ${0}         | ${2}          | ${'0'}
        ${0.0}       | ${2}          | ${'0'}
        ${78}        | ${2}          | ${'78'}
        ${11.1}      | ${2}          | ${'11,1'}
        ${11.1}      | ${10}         | ${'11,1'}
        ${11.123}    | ${1}          | ${'11,1'}
        ${11.123}    | ${2}          | ${'11,12'}
        ${11.123}    | ${3}          | ${'11,123'}
        ${11.123}    | ${100}        | ${'11,123'}
        ${11.129}    | ${2}          | ${'11,13'}
    `('should return `expected` if passed value is $value and decimal digits is $decimalDigits',
        ({value, decimalDigits, expected}) => {

        expect(formatNumericValue(value, {maxDecimalDigits: decimalDigits})).toBe(expected);
    });
});
