import {toNumber} from '../../src/helpers/toNumber';

describe('toNumber', () => {
    test.each`
        value        | decimalSeparator | expected
        ${''}        | ${','}           | ${0}
        ${undefined} | ${','}           | ${0}
        ${null}      | ${','}           | ${0}
        ${'0'}       | ${','}           | ${0}
        ${'0,0'}     | ${','}           | ${0}
        ${'0,0'}     | ${'_'}           | ${0}
        ${'0,0'}     | ${'.'}           | ${0}
        ${'10,0'}    | ${'.'}           | ${10}
        ${'10,0'}    | ${','}           | ${10}
        ${'10.1'}    | ${','}           | ${10.1}
        ${'10000_1'} | ${','}           | ${10000}
        ${'23,456'}  | ${','}           | ${23.456}
        ${'23,000'}  | ${','}           | ${23}
        ${'23_111'}  | ${'_'}           | ${23.111}
    `('should return `$expected` if passed value is $value, decimal separator `$decimalSeparator`',
        ({value, decimalSeparator, expected}) => {

        expect(toNumber(value, decimalSeparator)).toBe(expected);
    });
});
